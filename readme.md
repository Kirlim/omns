If you where searching for systems to store your quick notes, you just found....

# One More Notes System

A simple notes storing system that I'm developing for the sake of earning some experience with software development in C++. Information about the code can be found in (link to code.md).

This system is based on the _Zettelkasten_ method.

TODO : write about the intended structure in the system

 * Identifiers
 * Addresses
 * Tags
 * Not-Tags
 * Subject
 * Content

Why the `JsonMemoryDisk` repository? Putting it bluntly, I wanted to experiment with the idea of making my own little "database" to try a few things and most likely eventually regret the majority of my decisions. This is good technical experience. I'm trying to make it easy to add other repository implementations, though.