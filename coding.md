## Polemic Decisions

### Exceptions or Error Codes

I've been programming with Go daily, which is mostly based on error returns and it's quite easy to extend errors with codes with little effort. I've worked with exceptions before, on C#. I believe that, when used and documented correctly, exceptions and error codes are about the same and fits more into personal or design preferences. Even when execution speed is a must, both can be used correctly in a way that is also fast.

That being said, I've used error codes between shared libraries, but not exceptions. Exceptions between shared libraries sounds like a haunted cliff full of vengeful spirits ready to curse me and all generations that follows. I've chosen exceptions to earn experience in this area. Hopefully, if I ever regret this, the system will still be simple enought to migrate to error codes.

### Passing references to constructors

Some classes are receiving references on construction and stores the reference for use. The reference must outlive the instance lifetime. However, when reading the code it is unclear if the reference is used only by the Constructor or stored within the constructed instance; it may be a better choice to use any of the possible smart pointers instead. The hardest to answer question is if using a smart pointer, it should be `unique` or `shared` as these affect the caller ownership of the resources as well.