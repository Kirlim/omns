#include"../../catch.hpp"

#include<vector>

#include<omnsrepository/jsonmemorydisk/memoryjsonio.h>
#include<omnsrepository/jsonmemorydisk/jsonmemorydisk.h>


using namespace omns::repository;



namespace jsonmemorydisk_test
{
    /*
    const omns::Note coffeeNote {
        omns::NoteTitle{"About coffee"},
        omns::NoteContent{"Bitterly sweet drink"},
        std::vector<omns::NoteTag>{"cooking", "drinks"}
    };

    const auto coffeeNoteJson = R"(
        {
            "Version": 1,
            "Title": "About coffee",
            "Tags": ["cooking", "drinks"],
            "Content": "Bitterly sweet drink"
        }
    )";

    const omns::Note coldBrewNote {
        omns::NoteTitle{"Cold Brew"},
        omns::NoteContent{"For hot days"},
        std::vector<omns::NoteTag>{"idea"}
    };

    const auto coldBrewNoteJson = R"(
        {
            "Version": 1,
            "Title": "Cold Brew",
            "Tags": ["idea"],
            "Content": "For hot days"
        }
    )";

    const omns::Note withMilkNote {
        omns::NoteTitle{"With Milk"},
        omns::NoteContent{"Adds some calcium"},
        std::vector<omns::NoteTag>{"common"}
    };

    const auto withMilkNoteJson = R"(
        {
            "Version": 1,
            "Title": "With Milk",
            "Tags": ["common"],
            "Content": "Adds some calcium"
        }
    )";



    SCENARIO("Constructing a repository", "[JsonMemoryDiskRepository]")
    {
        MemoryJsonIO jsonIO;

        GIVEN("An IO without notes json content to be loaded")
        {
            WHEN("I create a new repository")
            {
                JsonMemoryDiskRepository repository{jsonIO};

                THEN("No notes will be loaded into the repository")
                {
                    REQUIRE(repository.GetNotesCount() == 0);
                }
            }
        }

        GIVEN("An IO with notes json content that can be loaded")
        {
            jsonIO.contents.emplace_back(NoteId{"note1"}, coffeeNoteJson);
            jsonIO.contents.emplace_back(NoteId{"note2"}, coldBrewNoteJson);

            WHEN("I create a new repository")
            {
                JsonMemoryDiskRepository repository{jsonIO};

                THEN("All json content are loaded into the repository")
                {
                    REQUIRE(repository.GetNotesCount() == 2);
                    REQUIRE(repository.GetNote("1") == coffeeNote);
                    REQUIRE(repository.GetNote("1.1") == coldBrewNote);
                }
            }
        }
    }



    SCENARIO("Getting notes that does not exists", "[JsonMemoryDiskRepository]")
    {
        GIVEN("Repository with notes")
        {
            MemoryJsonIO jsonIO;
            jsonIO.contents.emplace_back(NoteId{"note1"}, coffeeNoteJson);

            WHEN("I search for a note address that is not registered")
            {
                JsonMemoryDiskRepository repository{jsonIO};

                THEN("No notes will be returned")
                {
                    REQUIRE_FALSE(repository.GetNote("100.1"));
                }
            }
        }
    }



    SCENARIO("Creating notes", "[JsonMemoryDiskRepository]")
    {
        GIVEN("A repository of notes")
        {
            MemoryJsonIO jsonIO;

            WHEN("I create a note and then attempt to get it back")
            {
                JsonMemoryDiskRepository repository{jsonIO};
                repository.Save(coffeeNote);
                const auto recoveredNote = repository.GetNote(coffeeNote.Address);

                THEN("The newly created note was persisted")
                {
                    REQUIRE(jsonIO.contents.size() == 1);
                    // FIXME : how to verify that the contents is actually the note?
                }

                THEN("The newly created note will be returned")
                {
                    REQUIRE(recoveredNote == coffeeNote);
                }
            }

            // TODO : test on failing to create a note with an address that already exists
            // WHEN("I create two notes with the same address")
        }
    }
    */
}