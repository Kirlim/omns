#include"../catch.hpp"

#include<omns/omns.h>
#include<omnsrepository/jsonmemorydisk/memoryjsonio.h>
#include<omnsrepository/jsonmemorydisk/jsonmemorydisk.h>

using namespace omns;



namespace omns_test
{
    /*
    SCENARIO("Preparing a notes workplace", "[Omns]")
    {
        repository::MemoryJsonIO jsonIO;

        GIVEN("A new workplace without any notes")
        {
            repository::JsonMemoryDiskRepository repository{jsonIO};

            WHEN("A new instance of Omns is prepared")
            {
                Omns system{repository};
                system.Prepare();

                THEN("A new note of address 0 and title 'New Notes' will be created")
                {
                    REQUIRE(repository.GetNote("0") == Note{
                        NewlyCreatedRootAddress,
                        NoteClass("new"),
                        NoteTitle("New Notes"),
                        NoteContent("Newly created notes are listed under this note!"),
                        {}
                    });
                }
            }
        }
    }
    */
}
