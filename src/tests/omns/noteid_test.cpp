#include"../catch.hpp"

#include<omns/noteid.h>

using namespace omns;



namespace noteid_test
{
	TEST_CASE("NewEmptyId", "[NoteId]")
	{
		auto emptyId = NoteId::NewEmptyId();
		NoteId::IdType expectedId{ "0000000000000000000000000000000" };
		REQUIRE(emptyId.Id == expectedId);
	}



	TEST_CASE("NewRandomId", "[NoteId]")
	{
		auto randomId = NoteId::NewRandomId();
		auto emptyId = NoteId::NewEmptyId();
		REQUIRE(randomId != emptyId);

		auto anotherRandomId = NoteId::NewRandomId();
		REQUIRE(randomId != anotherRandomId);
	}
}
