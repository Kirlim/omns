#pragma once

#include<array>



namespace omns
{
	struct NoteId
	{
		using IdType = std::array<char, 32>;

		const IdType Id;

		NoteId(IdType);
		NoteId(const NoteId&) = default;
		NoteId(NoteId&&) = default;

		static auto NewRandomId()->NoteId;
		static auto NewEmptyId()->NoteId;

		auto operator<=>(const NoteId&) const = default;
	};
}