#pragma once



namespace omns
{
	struct Note;

	class INoteRepository
	{
	public:
		virtual ~INoteRepository() = default;

		// Creates the given note. The operation will fail if a note with the same
		// address is already registered
		// TODO : throw and document expected exception
		virtual auto Save(const Note&) -> void = 0;
	};
}