#pragma once

#include"../omns/notetag.h"



namespace omns
{
    class INoteRepository;

    const NoteTag NewlyCreatedRootAddress = "0";



    // The main class that represents one Omns execution context. This class acts
    // like a facade to all the underlying logic in dealing with notes.
    class Omns
    {
    public:
        // Starts an Omns execution context with the given @noteRepository. The noteRepository
        // must be guaranteed to outlive the Omns execution context.
        // The @noteRepository is assumed to be ready-to-use. Any needed previous setup or loading
        // must have already been done beforehand.
        explicit Omns(INoteRepository& noteRepository);
        
        // Check and does missing preparations that are need for Omns to
        // work properly.
        //  - Create note of address "0", needed as a root for newly created notes.
        auto Prepare() -> void;



    private:
        INoteRepository& m_NoteRepository;
    };
}