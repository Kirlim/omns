#pragma once

#include<vector>

#include"notetitle.h"
#include"notecontent.h"
#include"notetag.h"



namespace omns
{
	struct Note
	{
		NoteContent Content;
		NoteTitle Title;
		std::vector<NoteTag> Tags;

		Note(
			NoteTitle Title,
			NoteContent Content,
			std::vector<NoteTag> Tags
		);

		Note() = default;
		Note(const Note&) = default;
		Note(Note&&) = default;
	};



	auto operator==(const Note& lhs, const Note& rhs) -> bool;
}