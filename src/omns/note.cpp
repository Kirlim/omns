#include"include/omns/note.h"



namespace omns
{
	Note::Note(
		NoteTitle Title,
		NoteContent Content,
		std::vector<NoteTag> Tags
	) :
		Title(Title),
		Content(Content),
		Tags(Tags)
	{
	}



	auto operator==(const Note& lhs, const Note& rhs) -> bool
	{
		return lhs.Title == rhs.Title &&
			lhs.Content == rhs.Content &&
			lhs.Tags == rhs.Tags;
	}
}