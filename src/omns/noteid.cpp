#include"include/omns/noteid.h"

#include<sstream>
#include<random>



namespace omns
{
	// random_char and generate_hex are snippets from Bret Lowrey post "UUID Genaration in C++11"
	// https://lowrey.me/guid-generation-in-c-11/
	// Modified for specific id sizes

	auto random_char() -> unsigned int
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(0, 255);
		return dis(gen);
	}



	auto genereateRandomId() -> NoteId::IdType
	{
		constexpr char hexChars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		NoteId::IdType id;

		for (std::size_t i = 0; i < id.size() / 2; ++i)
		{
			const auto rc = random_char();
			id[i] = hexChars[(rc & 0xF0) >> 4];
			id[i+1] = hexChars[(rc & 0x0F) >> 0];
		}

		return id;
	}



	NoteId::NoteId(IdType id) :
		Id(id)
	{
	}



	auto NoteId::NewRandomId()->NoteId
	{
		return NoteId{ genereateRandomId() };
	}



	auto NoteId::NewEmptyId()->NoteId
	{
		return NoteId{ NoteId::IdType{"0000000000000000000000000000000"} };
	}
}