#pragma once

#include"ijsonio.h"



namespace omns::repository
{
    // Allows working with separate json files as notes. The id of each note is the filename
    // without the ".json" extension.
    class DiskJsonIO : public IJsonIO
    {
    public:
        explicit DiskJsonIO(const std::string directory);

        // Will load all files with ".json" extension in the provided directory on
        // construction.
        auto LoadAll(std::function<void(const NoteId&, const std::string contents)> setFn) -> void override;
        auto Save(const NoteId&, const std::string& jsonContents) -> void override;



    private:
        const std::string m_Directory;
    };
}