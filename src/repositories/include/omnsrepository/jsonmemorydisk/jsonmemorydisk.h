#pragma once

#include<vector>

#include<omns/inoterepository.h>

#include"ijsonio.h" // NoteId



namespace omns
{
	struct Note;
}

namespace omns::repository
{
	// A simple repository that acts by loading all .json files from a
	// directory into a memory format, and then changing the files on disk
	// as needed. The directory must contain only json files created by
	// this repository, in order to avoid undefined behavior. This repository
	// in not meant to large bases of notes or large contents; the memory consumption
	// will be high and performance may degrade quickly.
	class JsonMemoryDiskRepository : public INoteRepository
	{
	public:
		explicit JsonMemoryDiskRepository(IJsonIO& jsonIO);

		auto Save(const Note&) -> void override;

		auto GetNotesCount() const -> int;



	private:
		// TODO : m_JsonIO should be a std::unique_ptr!
		IJsonIO& m_JsonIO;

		std::vector<std::pair<NoteId, omns::Note>> m_Notes;
	};
}