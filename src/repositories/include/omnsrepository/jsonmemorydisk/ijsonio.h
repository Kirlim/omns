#pragma once

#include<functional>

#include<omns/note.h>



namespace omns::repository
{
    // Since the note address may not be the same as the persistence ID,
    // strings are used as different node id.
    using NoteId = std::string;



    // Represents input from and output to a mean of a json collection,
    // such as disk or memory. This is used by the Repository in order to
    // load, create, update and delete notes. Since the note address may not
    // be the same as the persistence ID, strings are used as note ids.
    //
    // TODO : think if actually it shouldn't be implementations of this class to handle json! Because it would make sense.
    class IJsonIO
    {
    public:
        virtual ~IJsonIO() = default;

        virtual auto LoadAll(std::function<void(const NoteId&, const std::string contents)> setFn) -> void = 0;
        virtual auto Save(const NoteId&, const std::string& jsonContents) -> void = 0;
    };
}