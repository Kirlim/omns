#pragma once

#include<vector>

#include"ijsonio.h"



namespace omns::repository
{
    // Used to aid with automatic testing
    class MemoryJsonIO : public IJsonIO
    {
    public:
        auto LoadAll(std::function<void(const NoteId&, const std::string contents)> setFn) -> void override;
        auto Save(const NoteId&, const std::string& jsonContents) -> void override;



    public:
        std::vector<std::pair<NoteId, std::string>> contents;
    };
}