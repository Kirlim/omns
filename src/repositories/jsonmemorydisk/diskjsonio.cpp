#include"../include/omnsrepository/jsonmemorydisk/diskjsonio.h"

#include<iostream>
#include<filesystem>
#include<fstream>
#include<sstream>
#include<string>



namespace omns::repository
{
	DiskJsonIO::DiskJsonIO(const std::string directory) :
		m_Directory(directory)
	{
	}



	auto DiskJsonIO::LoadAll(std::function<void(const NoteId&, const std::string contents)> setFn) -> void
	{
		// TODO : exceptions handling
		for (const auto& p : std::filesystem::directory_iterator(m_Directory))
		{
			std::ifstream file{ p.path(), std::ifstream::in };
			std::stringstream strStream;

			strStream << file.rdbuf();
			setFn(p.path().string(), strStream.str());
		}
	}



	auto DiskJsonIO::Save(const NoteId& id, const std::string& jsonContents) -> void
	{
		// TODO : exceptions handling
		const auto filename = m_Directory + "/" + id + ".json";
		std::ofstream file(filename.c_str(), std::ios::trunc);
		file << jsonContents;
	}
}