#include"../include/omnsrepository/jsonmemorydisk/jsonmemorydisk.h"

#include<sstream>
#include<random>
#include<string>
#include<algorithm>

#include<omns/note.h>

#include"../nlohmann/json.hpp"



namespace omns::repository
{
	// random_char and generate_hex are snippets from Bret Lowrey post "UUID Genaration in C++11"
	// https://lowrey.me/guid-generation-in-c-11/

	unsigned int random_char()
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(0, 255);
		return dis(gen);
	}

	std::string generate_hex(const unsigned int len)
	{
		std::stringstream ss;
		for (auto i = 0; i < len; i++)
		{
			const auto rc = random_char();
			std::stringstream hexstream;
			hexstream << std::hex << rc;
			auto hex = hexstream.str();
			ss << (hex.length() < 2 ? '0' + hex : hex);
		}
		return ss.str();
	}



	auto generateId() -> std::string
	{
		return generate_hex(16);
	}



	JsonMemoryDiskRepository::JsonMemoryDiskRepository(IJsonIO& jsonIO) :
		m_JsonIO(jsonIO)
	{
		// TODO : handle possible exceptions
		m_JsonIO.LoadAll([this](auto noteId, auto noteContents) {
			// TODO : translate exceptions to an omns exception
			auto parsed = nlohmann::json::parse(noteContents);

			// TODO : throw exception on unexpected json version
			auto jsonVersion = parsed["Version"];

			auto noteAddress = parsed["Address"];
			auto noteClass = parsed["Class"];
			auto noteTitle = parsed["Title"];
			auto noteContent = parsed["Content"];
			auto noteTags = parsed["Tags"];

			m_Notes.emplace_back(
				noteId,
				omns::Note{
					noteTitle,
					noteContent,
					noteTags
				}
			);
		});
	}



	auto JsonMemoryDiskRepository::Save(const Note& note) -> void
	{
		// TODO : get unexpected exceptions and handle them appropriately

		// TODO : re-enable when adding ID as strong concept
		//if (GetNote(note.Address)) {
		//    // TODO : throw exception and add test case
		//}

		// TODO : use ulid as id
		// TODO : reconsider when notes starts having their own ID
		const NoteId noteId = "someID.json";

		// FIXME : should be able to keep invariants if saving fails!
		m_Notes.emplace_back(noteId, note);

		nlohmann::json noteJson = {
			{"Version", "1"},
			{"Title", note.Title},
			{"Content", note.Content},
			{"Tags", note.Tags}
		};

		// TODO : make sure ID is free!
		m_JsonIO.Save(generateId(), noteJson.dump());
	}



	auto JsonMemoryDiskRepository::GetNotesCount() const -> int
	{
		return int(m_Notes.size());
	}
}