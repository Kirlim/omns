#include"../include/omnsrepository/jsonmemorydisk/memoryjsonio.h"



namespace omns::repository
{
    auto MemoryJsonIO::LoadAll(std::function<void(const NoteId&, const std::string contents)> setFn) -> void
    {
        for(const auto& noteInfo : contents)
        {
            setFn(noteInfo.first, noteInfo.second);
        }
    }



    auto MemoryJsonIO::Save(const NoteId& id, const std::string& jsonContents) -> void
    {
        contents.emplace_back(id, jsonContents);
    }
}